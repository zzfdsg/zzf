package com.example.demo.dao;

import com.example.demo.entities.Mark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MarkDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Mark> getAll(){
        return jdbcTemplate.query("select * from mark",new BeanPropertyRowMapper<>(Mark.class));
    }
    //添加
    public int addMark(Mark mark){
        return jdbcTemplate.update("INSERT INTO mark(sID,cID,Time,Score)VALUES (?,?,?,?)",mark.getsID(),mark.getcID(),mark.getTime(),mark.getScore());
    }
    //更新
    public int updateMark(Mark mark){
        return jdbcTemplate.update("UPDATE  mark SET cID=?,Time=?,Score=? where sID=?",mark.getcID(),mark.getTime(),mark.getScore(),mark.getsID());
    }
    //删除
    public int deleteMarkById(int sID){
        return jdbcTemplate.update("DELETE FROM mark WHERE sID=?",sID);
    }
    //查询
    public Mark getMarkById(int sID){
        return jdbcTemplate.queryForObject("select * from mark where sID=?",new BeanPropertyRowMapper<>(Mark.class),sID);
    }
}
