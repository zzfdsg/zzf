package com.example.demo.dao;

import com.example.demo.entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<User> getAll(){
        return jdbcTemplate.query("select * from user",new BeanPropertyRowMapper<>(User.class));
    }
    //添加
    public int addUser(User user){
        return jdbcTemplate.update("INSERT INTO user (id,name,pwd,lesson,sex) VALUES (?,?,?,?,?)",user.getId(),user.getName(),user.getPwd(),user.getLesson(),user.getSex());
    }
    //更新
    public int updateUser(User user){
        return jdbcTemplate.update("UPDATE user SET name=?,pwd=?,lesson=?,sex=? where id=?",user.getName(),user.getPwd(),user.getLesson(),user.getSex(),user.getId());
    }
    //删除
    public int deleteUserById(int id){
        return jdbcTemplate.update("DELETE FROM user WHERE id=?",id);
    }
    //查询
    public User getUserById(int id){
        return jdbcTemplate.queryForObject("select * from user where id=?",new BeanPropertyRowMapper<>(User.class),id);
    }

}
