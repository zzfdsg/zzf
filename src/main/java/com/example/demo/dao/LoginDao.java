package com.example.demo.dao;

import com.example.demo.entities.Login;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class LoginDao {
    private List<Login> logins;
    public LoginDao() {
        logins = new ArrayList<>();
        Login login = new Login();
        login.setUserName("admin");
        login.setPassword("123123");
        logins.add(login);
        Login login1 = new Login();
        login1.setUserName("zzf");
        login1.setPassword("111111");
        logins.add(login1);
        Login login2 = new Login();
        login2.setUserName("lml");
        login2.setPassword("123456");
        logins.add(login2);

    }
    public boolean verify(Login login){
        for(Login u: logins){
            if(u.getUserName().equals(login.getUserName()) && u.getPassword().equals(login.getPassword())){
                return true;
            }
        }
        return false;
    }
}
