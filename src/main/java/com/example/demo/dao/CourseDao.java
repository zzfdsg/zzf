package com.example.demo.dao;

import com.example.demo.entities.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CourseDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Course> getAll(){
        return jdbcTemplate.query("select * from course",new BeanPropertyRowMapper<>(Course.class));
    }
    //添加
    public int addCourse(Course course){
        return jdbcTemplate.update("INSERT INTO course(cName,tName)VALUES (?,?)",course.getcName(),course.gettName());
    }
    //更新
    public int updateCourse(Course course){
        return jdbcTemplate.update("UPDATE  course SET cName=?,tName=? where id=?",course.getcName(),course.gettName(),course.getId());
    }
    //删除
    public int deleteCourseById(int id){
        return jdbcTemplate.update("DELETE FROM course WHERE id=?",id);
    }
    //查询
    public Course getCourseById(int id){
        return jdbcTemplate.queryForObject("select * from course where id=?",new BeanPropertyRowMapper<>(Course.class),id);
    }
}
