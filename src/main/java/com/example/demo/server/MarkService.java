package com.example.demo.server;

import com.example.demo.dao.MarkDao;
import com.example.demo.entities.Mark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarkService {
    @Autowired
    private MarkDao markDao;
    public List<Mark> getAllMarks(){
        return markDao.getAll();
    }
    public int add(Mark mark){
        return markDao.addMark(mark);
    }
    public int update(Mark mark){
        return markDao.updateMark(mark);
    }
    public int deleteById(int sID){
        return markDao.deleteMarkById(sID);
    }
    public Mark getById(int sID){
        return markDao.getMarkById(sID);
    }
}
