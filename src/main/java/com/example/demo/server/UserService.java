package com.example.demo.server;

import com.example.demo.dao.UserDao;
import com.example.demo.entities.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;
    public List<User> getAllUsers(){
        return userDao.getAll();
    }
    public int add(User user){
        return userDao.addUser(user);
    }
    public int update(User user){
        return userDao.updateUser(user);
    }
    public int deleteById(int id){
        return userDao.deleteUserById(id);
    }
    public User getById(int id){
        return userDao.getUserById(id);
    }
}
