package com.example.demo.controller;

import com.example.demo.entities.Mark;
import com.example.demo.server.MarkService;
import com.example.demo.util.GeneralResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MarkController {
    @Autowired
    private MarkService markService;
    @GetMapping("/marks")
    public GeneralResponse books(){
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        response.setData(markService.getAllMarks());
        return response;
    }
    @PostMapping("/mark/add")
    public GeneralResponse add(@RequestParam int sID, @RequestParam int cID,@RequestParam String Time,@RequestParam int Score){
        Mark mark=new Mark();
        mark.setsID(sID);
        mark.setcID(cID);
        mark.setTime(Time);
        mark.setScore(Score);
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        response.setData(markService.add(mark));
        return response;
    }
    @PutMapping("/mark/update")
    public GeneralResponse update(@RequestParam int sID, @RequestParam int cID,@RequestParam String Time,@RequestParam int Score){
        Mark mark=new Mark();
        mark.setsID(sID);
        mark.setcID(cID);
        mark.setTime(Time);
        mark.setScore(Score);
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        response.setData(markService.update(mark));
        return response;
    }

    @PostMapping("/mark/delete")
    public GeneralResponse delete(@RequestParam int sID){
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        response.setData(markService.deleteById(sID));
        return response;
    }
    @GetMapping("/mark")
    public GeneralResponse bookById(@RequestParam int sID){
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        List<Mark> result=new ArrayList<>();
        result.add(markService.getById(sID));
        response.setData(result);
        return response;
    }

}
