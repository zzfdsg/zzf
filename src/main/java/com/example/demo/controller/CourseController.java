package com.example.demo.controller;

import com.example.demo.entities.Course;
import com.example.demo.server.CourseService;
import com.example.demo.util.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CourseController {
    @Autowired
    private CourseService courseService;
    @GetMapping("/courses")
    public GeneralResponse books(){
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        response.setData(courseService.getAllCourses());
        return response;
    }
    @PostMapping("/course/add")
    public GeneralResponse add(@RequestParam String cName, @RequestParam String tName){
        Course course=new Course();
        course.setcName(cName);
        course.settName(tName);

        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        response.setData(courseService.add(course));
        return response;
    }
    @PutMapping("/course/update")
    public GeneralResponse update(@RequestParam String cName, @RequestParam String tName,@RequestParam int id){
        Course course=new Course();
        course.setId(id);
        course.setcName(cName);
        course.settName(tName);
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        response.setData(courseService.update(course));
        return response;
    }

    @PostMapping("/course/delete")
    public GeneralResponse delete(@RequestParam int id){
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        response.setData(courseService.deleteById(id));
        return response;
    }
    @GetMapping("/course")
    public GeneralResponse bookById(@RequestParam int id){
        GeneralResponse response=new GeneralResponse();
        response.setSuccess(true);
        List<Course> result=new ArrayList<>();
        result.add(courseService.getById(id));
        response.setData(result);
        return response;
    }

}
