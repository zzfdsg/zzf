package com.example.demo.controller;

import com.example.demo.dao.LoginDao;
import com.example.demo.entities.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
@RestController
public class LoginController {
    @Autowired
    private LoginDao loginDao;

    @RequestMapping("/index")
    public String index(){
        return "index";
}

    @GetMapping("/hello")
    public String hello(){
        int a = 1 / 0;
        return "the result is"+a;
    }
    @PostMapping("/login")
    public String login(String userName,String password,HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        Login u = new Login();
        u.setUserName(userName);
        u.setPassword(password);

        if(loginDao.verify(u) ){
            return "success";
        }else {
            return "fault";
        }
    }
    }
