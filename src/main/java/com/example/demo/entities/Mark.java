package com.example.demo.entities;

public class Mark {
    public int sID;
    public int cID;
    public String Time;
    public int Score;

    public int getsID() {
        return sID;
    }

    public void setsID(int sID) {
        this.sID = sID;
    }

    public int getcID() {
        return cID;
    }

    public void setcID(int cID) {
        this.cID = cID;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    @Override
    public String toString() {
        return "Mark{" +
                "sID=" + sID +
                ", cID=" + cID +
                ", Time='" + Time + '\'' +
                ", Score=" + Score +
                '}';
    }
}
