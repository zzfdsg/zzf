package com.example.demo.entities;

public class Course {
    public int id;
    public String cName;
    public String tName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", cName='" + cName + '\'' +
                ", tName='" + tName + '\'' +
                '}';
    }

    public static class User {
        public int id;
        public String name;
        public int pwd;
        public String lesson;
        public String sex;
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPwd() {
            return pwd;
        }

        public void setPwd(int pwd) {
            this.pwd = pwd;
        }

        public String getLesson() {
            return lesson;
        }

        public void setLesson(String lesson) {
            this.lesson = lesson;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }



        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", pwd=" + pwd +
                    ", lesson='" + lesson + '\'' +
                    ", sex='" + sex + '\'' +
                    '}';
        }
    }
}
