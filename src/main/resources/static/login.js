function check(){
    var userName =document.getElementById("userName").value;
    var password =document.getElementById("password").value;
    if(userName.length == 0){
        alert("用户名不能为空")
        location.href="index.html";

    }else if(password.length < 6){
        alert("密码不能小于6位")
    }else{
        getFormInfo()
    }
}
//第二种写法（参数写成json数据形式）
function getFormInfo(){
    $.ajax({
        url: "http://localhost:8080/login",
        type: "POST",
        //  async:false,
        headers: {
            "Content-type" :"application/x-www-form-urlencoded",
        },
        data:{
            userName:document.getElementById("userName").value,
            password:document.getElementById("password").value
        },
        cache:false,
        success: function(data){
            if(data=="success"){
                window.location.href = "./course.html";
                window.sessionStorage.setItem("userName", document.getElementById("userName").value,);
                window.sessionStorage.setItem("password", document.getElementById("password").value,);
                console.log("aaa");
            }else{
                alert("密码错误")
            }
            console.log(data);
        },
        error:function(err){
            console.log(err);
        }
    });
}