
function refresh1() {
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            procQueryResponse(this.responseText)
        }
    };
    xHttp.open("GET","/marks",true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function procQueryResponse(responseText) {
    const obj = JSON.parse(responseText);
    if(obj.success){
        const tblBody1 = document.getElementById('marks');
        tblBody1.innerHTML='';
        for(let mark of obj.data){
            const tr1 = document.createElement('tr');
            const td6 = document.createElement('td');
            td6.innerText=mark.sID;
            const td7 = document.createElement('td');
            td7.innerText=mark.cID;
            const td8 = document.createElement('td');
            td8.innerText=mark.Time;
            const td9 = document.createElement('td');
            td9.innerText=mark.Score;
            const td10 = document.createElement('td');
            const iptDel1=document.createElement('input');
            iptDel1.type='button';
            iptDel1.value='删除';
            iptDel1.addEventListener('click',function () {
                onDelete1(mark);
                return false;
            })
            const iptUpdate1=document.createElement('input');
            iptUpdate1.type='button';
            iptUpdate1.value='修改';
            iptUpdate1.addEventListener('click',function () {
                update1(mark);
                return false;
            })
            td10.appendChild(iptDel1);
            td10.appendChild(iptUpdate1);
            tr1.appendChild(td6);
            tr1.appendChild(td7);
            tr1.appendChild(td8);
            tr1.appendChild(td9);
            tr1.appendChild(td10);
            tblBody1.appendChild(tr1);
        }
    }

}
function onDelete1(mark) {
    console.log('删除中');
    console.log(mark);
    xHttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            const obj1 = JSON.parse(this.responseText);
            console.log(obj1);
            if (obj1.success) {
                alert('删除成功');
                refresh1();
            } else {
                alert('删除失败');
            }
        }
    }
    xHttp.open("POST","/mark/delete?sID=" + mark.sID,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function add1() {
    const iptsID=document.getElementById('iptsID');
    const iptcID=document.getElementById('iptcID');
    const iptTime=document.getElementById('iptTime');
    const iptScore=document.getElementById('iptScore');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            const obj = JSON.parse(this.responseText);
            console.log(obj);
            if(obj.success){
                alert('添加成功');
                refresh1();
            }else {
                alert('添加失败');
            }
        }
    }
    xHttp.open("POST","/mark/add?sID=" + iptsID.value + '&cID=' + iptcID.value+ '&Time=' + iptTime.value+ '&Score=' + iptScore.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function update1(mark) {
    console.log('更新中');
    const iptsID=document.getElementById('iptsID');
    const iptcID=document.getElementById('iptcID');
    const iptTime=document.getElementById('iptTime');
    const iptScore=document.getElementById('iptScore');
    iptsID.value=mark.sID;
    iptcID.value=mark.cID
    iptTime.value=mark.Time;
    iptScore.value=mark.Score;
}
function onUpdate1() {
    const iptsID=document.getElementById('iptsID');
    const iptcID=document.getElementById('iptcID');
    const iptTime=document.getElementById('iptTime');
    const iptScore=document.getElementById('iptScore');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            const obj = JSON.parse(this.responseText);
            console.log(obj);
            if(obj.success){
                alert('修改成功');
                refresh1();
            }else {
                alert('修改失败');
            }
        }
    }
    xHttp.open("PUT","/mark/update?sID=" + iptsID.value + '&cID=' + iptcID.value + '&Time=' + iptTime.value+'&Score=' + iptScore.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();

}
function getById1() {
    const iptsID=document.getElementById('iptsID');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            procQueryResponse(this.responseText)
        }
    };
    xHttp.open("GET","/mark?sID=" + iptsID.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
var xHttp = new XMLHttpRequest();
window.onload = function () {
    refresh1();
}

