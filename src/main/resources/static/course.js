function refresh() {
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            procQueryResponse(this.responseText)
        }
    };
    xHttp.open("GET","/courses",true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function procQueryResponse(responseText) {
    const obj = JSON.parse(responseText);
    if(obj.success){
        const tblBody = document.getElementById('courses');
        tblBody.innerHTML='';
        for(let course of obj.data){
            const tr = document.createElement('tr');
            const td1 = document.createElement('td');
            td1.innerText=course.id;
            const td2 = document.createElement('td');
            td2.innerText=course.cName;
            const td3 = document.createElement('td');
            td3.innerText=course.tName;
            const td4 = document.createElement('td');
            const iptDel=document.createElement('input');
            iptDel.type='button';
            iptDel.value='删除';
            iptDel.addEventListener('click',function () {
                onDelete(course);
                return false;
            })
            const iptUpdate=document.createElement('input');
            iptUpdate.type='button';
            iptUpdate.value='修改';
            iptUpdate.addEventListener('click',function () {
                update(course);
                return false;
            })
            td4.appendChild(iptDel);
            td4.appendChild(iptUpdate);
            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            tblBody.appendChild(tr);
        }


    }

}
function onDelete(course) {
    console.log('删除中');
    console.log(course);
    xHttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            const obj = JSON.parse(this.responseText);
            console.log(obj);
            if (obj.success) {
                alert('删除成功');
                refresh();
            } else {
                alert('删除失败');
            }
        }
    }
    xHttp.open("POST","/course/delete?id=" + course.id,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function add() {
    const iptName=document.getElementById('iptName');
    const iptAuthor=document.getElementById('iptAuthor');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            const obj = JSON.parse(this.responseText);
            console.log(obj);
            if(obj.success){
                alert('添加成功');
                refresh();
            }else {
                alert('添加失败');
            }
        }
    }
    xHttp.open("POST","/course/add?cName=" + iptName.value + '&tName=' + iptAuthor.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function update(course) {
    console.log('更新中');
    const iptName=document.getElementById('iptName');
    const iptAuthor=document.getElementById('iptAuthor');
    const iptId=document.getElementById('iptId');
    iptId.value=course.id;
    iptName.value=course.cName;
    iptAuthor.value=course.tName;
}
function onUpdate() {
    const iptName=document.getElementById('iptName');
    const iptAuthor=document.getElementById('iptAuthor');
    const iptId=document.getElementById('iptId');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            const obj = JSON.parse(this.responseText);
            console.log(obj);
            if(obj.success){
                alert('修改成功');
                refresh();
            }else {
                alert('修改失败');
            }
        }
    }
    xHttp.open("PUT","/course/update?cName=" + iptName.value + '&tName=' + iptAuthor.value + '&id=' + iptId.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();

}
function getById() {
    const iptId=document.getElementById('iptId');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            procQueryResponse(this.responseText)
        }
    };
    xHttp.open("GET","/course?id=" + iptId.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function run(){
    window.location.href='index1.html';
}
var xHttp = new XMLHttpRequest();
window.onload = function () {
    refresh();
}

