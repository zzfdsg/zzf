
function refresh2() {
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            procQueryResponse(this.responseText)
        }
    };
    xHttp.open("GET","/users",true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function procQueryResponse(responseText) {
    const obj = JSON.parse(responseText);
    if(obj.success){
        const tblBody2 = document.getElementById('users');
        tblBody2.innerHTML='';
        for(let user of obj.data){
            const tr2 = document.createElement('tr');
            const td11 = document.createElement('td');
            td11.innerText=user.id;
            const td12 = document.createElement('td');
            td12.innerText=user.name;
            const td13 = document.createElement('td');
            td13.innerText=user.pwd;
            const td14 = document.createElement('td');
            td14.innerText=user.lesson;
            const td15 = document.createElement('td');
            td15.innerText=user.sex;
            const td16 = document.createElement('td');
            const iptDel2=document.createElement('input');
            iptDel2.type='button';
            iptDel2.value='删除';
            iptDel2.addEventListener('click',function () {
                onDelete2(user);
                return false;
            })
            const iptUpdate2=document.createElement('input');
            iptUpdate2.type='button';
            iptUpdate2.value='修改';
            iptUpdate2.addEventListener('click',function () {
                update2(user);
                return false;
            })
            td16.appendChild(iptDel2);
            td16.appendChild(iptUpdate2);
            tr2.appendChild(td11);
            tr2.appendChild(td12);
            tr2.appendChild(td13);
            tr2.appendChild(td14);
            tr2.appendChild(td15);
            tr2.appendChild(td16);
            tblBody2.appendChild(tr2);
        }
    }

}
function onDelete2(user) {
    console.log('删除中');
    console.log(user);
    xHttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            const obj2 = JSON.parse(this.responseText);
            console.log(obj2);
            if (obj2.success) {
                alert('删除成功');
                refresh2();
            } else {
                alert('删除失败');
            }
        }
    }
    xHttp.open("POST","/user/delete?id=" + user.id,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function add2() {
    const id=document.getElementById('id');
    const name=document.getElementById('name');
    const pwd=document.getElementById('pwd');
    const lesson=document.getElementById('lesson');
    const sex=document.getElementById('sex');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            const obj = JSON.parse(this.responseText);
            console.log(obj);
            if(obj.success){
                alert('添加成功');
                refresh2();
            }else {
                alert('添加失败');
            }
        }
    }
    xHttp.open("POST","/user/add?id=" + id.value + '&name=' + name.value + '&pwd=' + pwd.value+'&lesson=' + lesson.value + '&sex=' + sex.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
function update2(user) {
    console.log('更新中');
    const id=document.getElementById('id');
    const name=document.getElementById('name');
    const pwd=document.getElementById('pwd');
    const lesson=document.getElementById('lesson');
    const sex=document.getElementById('sex');
    id.value=user.id;
    name.value=user.name;
    pwd.value=user.pwd;
    lesson.value=user.lesson;
    sex.value=user.sex;
}
function onUpdate2() {
    const id=document.getElementById('id');
    const name=document.getElementById('name');
    const pwd=document.getElementById('pwd');
    const lesson=document.getElementById('lesson');
    const sex=document.getElementById('sex');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            const obj = JSON.parse(this.responseText);
            console.log(obj);
            if(obj.success){
                alert('修改成功');
                refresh2();
            }else {
                alert('修改失败');
            }
        }
    }
    xHttp.open("PUT","/user/update?id=" + id.value + '&name=' + name.value + '&pwd=' + pwd.value+'&lesson=' + lesson.value+'&sex=' + sex.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();

}
function getById2() {
    const id=document.getElementById('id');
    xHttp.onreadystatechange = function (){
        if (this.readyState == 4 && this.status == 200){
            procQueryResponse(this.responseText)
        }
    };
    xHttp.open("GET","/user?id=" + id.value,true);
    xHttp.setRequestHeader('Accept','application/json');
    xHttp.send();
}
var xHttp = new XMLHttpRequest();
window.onload = function () {
    refresh2();
}

